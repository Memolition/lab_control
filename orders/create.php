<?php require_once('../res/php/session.php');
        require_once('../res/php/database.php');
        require_once('../res/php/strings.php');

if(isset($_POST['insert'])) {
        
} else {
        if(isset($_POST['patient']) && !empty($_POST['patient'])) {

?>
<div class="container-fluid">
<form action="#" class="create_order">
        <div class="input-group">
                <select class="form-control" name="hospital">
                        <option value="0" selected disabled><?php echo ORDER_HOSPITAL; ?></option>
                        <?php
                                if($hospital_select = $MySQLi->prepare("SELECT id, name FROM hospitals")) {
                                        $hospital_select->execute();
                                        $hospital_select->bind_result($hospital_id, $hospital_name);
                                        while($hospital_select->fetch()) {
                        ?>
                                <option value="<?php echo $hospital_id; ?>"><?php echo $hospital_name; ?></option>                        
                        <?php
                                        }
                                }
                        ?>
                </select>
        </div>
        <div class="input-group">
                <select class="form-control" name="doctor">
                        <option value="0" selected disabled><?php echo ORDER_DOCTOR; ?></option>
                        <?php
                                if($doctor_select = $MySQLi->prepare("SELECT id, name FROM doctors")) {
                                        $doctor_select->execute();
                                        $doctor_select->bind_result($doctor_id, $doctor_name);
                                        while($doctor_select->fetch()) {
                        ?>
                                <option value="<?php echo $doctor_id; ?>"><?php echo $doctor_name; ?></option>                        
                        <?php
                                        }
                                }
                        ?>
                </select>
        </div>
        <div class="input-group">
                <select id="order_price" class="form-control" name="price">
                        <option value="0" selected disabled><?php echo ORDER_PRICE; ?></option>
                        <?php
                                if($price_select = $MySQLi->prepare("SELECT id, name FROM prices")) {
                                        $price_select->execute();
                                        $price_select->bind_result($price_id, $price_name);
                                        while($price_select->fetch()) {
                        ?>
                                <option value="<?php echo $price_id; ?>"><?php echo $price_name; ?></option>                        
                        <?php
                                        }
                                }
                        ?>
                </select>
        </div>
        
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                        
                        

        <?php
                if($category_select = $MySQLi->prepare("SELECT id, name FROM test_categories ORDER BY name")) {
                        $category_select->execute();
                        $category_select->bind_result($category_id, $category_name);
                        $category_select->store_result();
                        while($category_select->fetch()) {
        ?>
                        <div class="panel-heading" role="tab" id="heading_<?php echo $category_id; ?>">
                                <h4 class="panel-title">
                                        <a role="button" class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse_<?php echo $category_id; ?>" aria-expanded="false" aria-controls="collapse_<?php echo $category_id; ?>">
                                                <?php echo $category_name; ?>
                                        </a>
                                </h4>
                        </div>
                        <div id="collapse_<?php echo $category_id; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading_<?php echo $category_id; ?>">
                                <div class="panel-body">
                                        <ul class="db_tests_list nav nav-pills nav-stacked">
                
        <?php
                                if($test_select = $MySQLi->prepare("SELECT id, name FROM tests WHERE category = ? ORDER BY name")) {
                                        $test_select->bind_param('d', $category_id);
                                        $test_select->execute();
                                        $test_select->store_result();
                                        if($test_select->num_rows > 0) {
                                                $test_select->bind_result($test_id, $test_name);
                                                while($test_select->fetch()) {
        ?>
                                                <li class="order_test_item" role="presentation" data-id="<?php echo $test_id; ?>"><a href="#"><?php echo $test_name; ?></a></li>
        <?php
                                                }
                                        }
                                        $test_select->free_result();
                                        $test_select->close();
                                }
        ?>
                                        </ul>
                                </div>
                        </div>
        <?php
                        }
                        $category_select->free_result();
                        $category_select->close();
                }
        ?>

                                
                </div>
        </div>
        <div class="container-fluid">
                <ul class="order_tests_list nav nav-pills nav-stacked">
                        <li class="order_test_template" role="presentation">
                                <span class="name"></span>
                                <div class="pull-right">
                                        <span class="test_price badge">
                                                Q. 0.00
                                        </span>
                                        <span class="remove_test badge">
                                                <span class="glyphicon glyphicon-trash"></span>
                                        </span>
                                </div>
                        </li>
                        <li id="order_total_tests" role="presentation" class="active"><a href="#">Total: <span class="total pull-right"></span></a></li>
                </ul>
        </div>
        <div class="input-group">
                <input class="form-control" type="submit" value="<?php echo ORDER_SUBMIT; ?>" />
        </div>
</form>
</div>
<?php
        }
}
?>
