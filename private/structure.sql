CREATE TABLE users (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        username VARCHAR(20) NOT NULL UNIQUE,
        password CHAR(60) NOT NULL,
        fname VARCHAR(50) NULL,
        lname VARCHAR(50) NULL,
        phone VARCHAR(50) NULL
);

CREATE TABLE patients (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        afiliated TINYINT(1) NOT NULL,
        code VARCHAR(20) NULL,
        fnames VARCHAR(100) NULL,
        lnames VARCHAR(100) NULL,
        age DATE NULL,
        address TINYTEXT NULL,
        phone VARCHAR(15) NULL
);

DELIMITER //
CREATE PROCEDURE search_patient (
        IN in_afiliated INT,
        IN in_afiliation_code VARCHAR(20),
        IN in_fname VARCHAR(100),
        IN in_lname VARCHAR(100),
        IN in_address TINYTEXT)
BEGIN
        SELECT * FROM patients p
        WHERE 
                (p.afiliated = in_afiliated OR in_afiliated IS NULL) AND
                (p.code = in_afiliation_code OR in_afiliation_code IS NULL) AND
                (p.fnames LIKE CONCAT('%', in_fname, '%') OR in_fname IS NULL) AND
                (p.lnames LIKE CONCAT('%', in_lname, '%') OR in_lname IS NULL) AND
                (p.address LIKE CONCAT('%', in_address, '%') OR in_address IS NULL)
        ;
END
//
DELIMITER ;

CREATE TABLE hospitals (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name varchar(100) NOT NULL,
        phone varchar(15) NULL,
        address TINYTEXT NULL
);

CREATE TABLE doctors (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name varchar(100) NOT NULL,
        phone varchar(15) NULL,
        address TINYTEXT NULL
);

CREATE TABLE orders (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        hospital INT NULL,
        doctor INT NULL,
        patient INT NOT NULL,
        date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
        tests TINYTEXT NULL,
        printable TINYINT(1) NOT NULL DEFAULT 0,
        print_date DATETIME NULL,
        price INT NOT NULL,
        user INT NULL
);

CREATE TABLE tests (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(150),
        prices TEXT,
        category INT
);

CREATE TABLE test_params (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        test INT NOT NULL,
        ord INT NOT NULL UNIQUE,
        name VARCHAR(50) NOT NULL,
        type INT NOT NULL,
        unique_value TINYTEXT NULL,
        minimum VARCHAR(50) NULL,
        maximum  VARCHAR(50) NULL,
        selection TEXT NULL,
        suggestions TEXT NULL,
        measurement INT NULL
);

CREATE TABLE test_categories (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(150) NOT NULL
);

CREATE TABLE test_prices (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(50) NOT NULL,
        price DECIMAL(6, 2) NOT NULL
);
