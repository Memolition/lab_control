<?php require_once('../res/php/session.php');
        require_once('../res/php/database.php');
        require_once('../res/php/strings.php');

$error = false;

if(isset($_POST['afiliated'])) {
        $afiliated = $_POST['afiliated'];
} else {
        $afiliated = 0;
}

if(isset($_POST['afiliation_code'])) {
        $code = $_POST['afiliation_code'];
} else {
        $code = NULL;
}

if(isset($_POST['fname'])) {
        $fnames = $_POST['fname'];
} else {
        $error = true;
}

if(isset($_POST['lname'])) {
        $lnames = $_POST['lname'];
} else {
        $error = true;
}

if(isset($_POST['age'])) {
        $age = $_POST['age'];
} else {
        $error = true;
}

if(isset($_POST['address']) || isset($_POST['phone'])) {
        if(isset($_POST['address'])) {
                $address = $_POST['address'];
        } else {
                $address = NULL;
        }
        if(isset($_POST['phone'])) {
                $phone = $_POST['phone'];
        } else {
                $phone = NULL;
        }
} else {
        $error = true;
}

if(!$error) {
        if($insert_patient = $MySQLi->prepare("INSERT INTO patients (afiliated, code, fnames, lnames, age, address, phone) VALUES (?,?,?,?,?,?,?)")) {
                $insert_patient->bind_param('dssssss', $afiliated, $code, $fnames, $lnames, $age, $address, $phone);
                $insert_patient->execute();
                echo '0Patient inserted successfully.';
        } else {
                echo '1An error ocurred inserting in db, please try again.';
        }
} else {
        echo '1An error ocurred, please try again.';
}
?>
