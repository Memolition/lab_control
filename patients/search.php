<?php require_once('../res/php/session.php');
        require_once('../res/php/database.php');
        require_once('../res/php/strings.php');
if(isset($_POST['search']) && $_POST['search'] == true) {
        if($search_statement = $MySQLi->prepare("CALL search_patient(?,?,?,?,?)")) {
                echo 0;
                if(isset($_POST['afiliation_code']) && !empty($_POST['afiliation_code'])) {
                        $afiliation_code = $_POST['afiliation_code'];
                } else {
                        $afiliation_code = NULL;
                }
                
                if(isset($_POST['fname']) && !empty($_POST['fname'])) {
                        $fname = $_POST['fname'];
                } else {
                        $fname = NULL;
                }
                if(isset($_POST['lname']) && !empty($_POST['lname'])) {
                        $lname = $_POST['lname'];
                } else {
                        $lname = NULL;
                }
                if(isset($_POST['address']) && !empty($_POST['address'])) {
                        $address = $_POST['address'];
                } else {
                        $address = NULL;
                }
                $search_statement->bind_param('issss', $afiliated = 0, $afiliation_code, $fname, $lname, $address);
                

                $search_statement->execute();
                $search_statement->store_result();
                if($search_statement->num_rows > 0) {
                        $search_statement->bind_result($id, $afiliated, $code, $fnames, $lnames, $age, $address, $phone);
?>
<table class="patient_search_list table table-bordered table-striped">
        <thead class="thead-inverse">
                <tr>
                        <th><?php echo PATIENT_AFILIATED_CODE; ?></th>
                        <th><?php echo PATIENT_FNAMES; ?></th>
                        <th><?php echo PATIENT_LNAMES; ?></th>
                        <th><?php echo PATIENT_AGE; ?></th>
                        <th><?php echo PATIENT_ADDRESS; ?></th>
                        <th><?php echo PATIENT_PHONE; ?></th>
                </tr>
        </thead>
        <tbody>
<?php
                        while($search_statement->fetch()) {
?>
                <tr data-id="<?php echo $id; ?>" class="patient_row">
                        <td><?php echo $code ? $code : "--"; ?></td>
                        <td><?php echo $fnames ? $fnames : "--"; ?></td>
                        <td><?php echo $lnames ? $lnames : "--"; ?></td>
                        <td><?php echo $age; ?></td>
                        <td><?php echo $address ? $address : "--"; ?></td>
                        <td><?php echo $address ? $phone : "--"; ?></td>
                </tr>
<?php
                        }
                } else {
?>
<div class="panel">
        <h2><?php echo MISC_SEARCH_NO_RESULTS; ?></h2>
        <button type="button" class="insert_patient_submit btn btn-primary"><?php echo PATIENT_SAVE_NEW; ?></button>
        <div class="patient_insert_warning alert alert-danger" role="alert"></div>
        <div class="patient_insert_success alert alert-success" role="alert"></div>
<?php                        
                }
        } else {
                echo 1;
                echo "An error ocurred, please try reloading.";
        }
?>
        </tbody>
</table>
<?php
} else {
?>
<div class="container-fluid">
        <form class="search_patient" action="#">
                <div class="checkbox">
                        <label>
                                <input type="checkbox" class="afiliated" name="afiliated"> <?php echo PATIENT_AFILIATED; ?>
                        </label>
                </div>
                <div class="input-group afiliation_code_group">
                        <input type="text" class="form-control" name="afiliation_code"  placeholder="<?php echo PATIENT_AFILIATED_CODE; ?>" />
                </div>
                <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-user"></span></div>
                        <input type="text" class="form-control" name="fname" placeholder="<?php echo PATIENT_FNAMES; ?>" />
                        <input type="text" class="form-control" name="lname" placeholder="<?php echo PATIENT_LNAMES; ?>" />
                </div>
                <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></div>
                        <input type="date" class="form-control" name="age" placeholder="<?php echo PATIENT_AGE; ?>" />
                </div>
                <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-home"></span></div>
                        <input type="text" class="form-control" name="address" placeholder="<?php echo PATIENT_ADDRESS; ?>" />
                </div>
                <div class="input-group">
                        <div class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></div>
                        <input type="text" class="form-control" name="phone" placeholder="<?php echo PATIENT_PHONE; ?>" />
                </div>
                <button type="submit" class="btn btn-default patient_search_submit"><?php echo MISC_SEARCH; ?></button>
        </form>
</div>

<div class="patient_search_warning alert alert-danger" role="alert"></div>
<div class="container-fluid patient_search_response"></div>
<?php
}
?>
