/*Global*/
const LOG_IN = "Log in";
const LOGGIN_IN = "Loggin in...";
const SEARCHING = "Searching...";
const SEARCH = "Search";
const INSERTING = "Inserting...";
const INSERT = "Save as new patient";
const CREATE = "Create Order";

$(function () {
        $('[data-toggle="tooltip"]').tooltip()

        $(document).on('click', '.logout_trigger', function() {
                $('#logout_modal').modal('show');
                setTimeout(function() {
                        $.post('users/logout.php', function(r) {
                                location.reload();
                        });
                }, 1000);
        });

        $(document).on('submit', '.login_form', function(e) {
                e.preventDefault();
                $('.login_submit').text(LOGGIN_IN).prop('disabled', true);

                $('.login_success').hide('blind');
                $('.login_warning').hide('blind');
                $.post('users/login.php', $(this).serialize(), function(r) {
                        if(r.substring(0,1) == 0) {
                                $('.login_success').text(r.substring(1, r.length - 1)).show('blind');
                                $('.login_submit').text(LOG_IN).prop('disabled', false);
                                location.reload();
                        } else {
                                $('.login_warning').text(r.substring(1, r.length - 1)).show('blind');
                                $('.login_submit').text(LOG_IN).prop('disabled', false);
                        }
                });
        });

        $(document).on('click', '.search_patient_navbar', function(e) {
                $('#main_container').hide('blind', function() {
                        $.post('patients/search.php', function(r) {
                                $('#main_container').html(r);
                                $('#main_container').show('blind');
                        });
                });
        });
        $(document).on('submit', '.search_patient', function(e) {
                e.preventDefault();
                $('.patient_search_submit').text(SEARCHING).prop('disabled', true);

                $('.patient_search_response').hide('blind');
                $('.patient_search_warning').hide('blind');
                $.post('patients/search.php', "search=true&" + $(this).serialize(), function(r) {
                        if(r.substring(0,1) == 0) {
                                $('.patient_search_response').html(r.substring(1, r.length)).show('blind');
                                $('.patient_search_submit').text(SEARCH).prop('disabled', false);
                        } else {
                                $('.patient_search_warning').text(r.substring(1, r.length)).show('blind');
                                $('.patient_search_submit').text(SEARCH).prop('disabled', false);
                        }
                });
        });
        
        $(document).on('change', '.afiliated', function(e) {
                $('.afiliation_code_group').toggle(this.checked);
        });
        
        $(document).on('click', '.insert_patient_submit', function(e) {
                
                var error = false;
                var error_string = "An error ocurred:<br/>";
                $('.patient_insert_warning').hide();
                if($('.search_patient')) {
                        $('.insert_patient_submit').text(INSERTING).prop('disabled', true);
                        if(!$('.search_patient').find('[name=fname]').first().val().trim().length > 0) {
                                error = true;
                                error_string += " - Please write the patient's first name.<br/>";
                        }
                        if(!$('.search_patient').find('[name=lname]').first().val().trim().length > 0) {
                                error = true;
                                error_string += " - Please write the patient's last name.<br/>";
                        }
                        if(!$('.search_patient').find('[name=age]').first().val().trim().length > 0) {
                                error = true;
                                error_string += " - Please write the patient's age or date of birth.<br/>";
                        }
                        if(!$('.search_patient').find('[name=address]').first().val().trim().length > 0
                                && !$('.search_patient').find('[name=phone]').first().val().trim().length > 0) {
                                error = true;
                                error_string += " - Please write the patient's address or phone number.<br/>";
                        }
                        if(!error) {
                                $.post('patients/create.php', $('.search_patient').serialize(), function(r) {
                                        if(r.substring(0,1) == 0) {
                                                $('.patient_insert_success').html(r.substring(1, r.length)).show('blind');
                                                $('.insert_patient_submit').text(INSERT).prop('disabled', false);
                                                $('#main_container').hide('blind', function() {
                                                        $.post('orders/create.php', function(r) {
                                                                $('#main_container').html(r);
                                                                $('#main_container').show('blind');
                                                        });
                                                });
                                        } else {
                                                $('.patient_insert_warning').text(r.substring(1, r.length)).show('blind');
                                                $('.insert_patient_submit').text(INSERT).prop('disabled', false);
                                        }
                                });
                        } else {
                                $('.patient_insert_warning').html(error_string).show('blind');
                        }
                } else {
                        $('.patient_insert_warning').text('An error ocurred, please try again.').show('blind');
                }
        });
        
        $(document).on('click', '.patient_row', function(e) {
                if($(this).attr('data-id') && $(this).attr('data-id').length) {
                        var patient_id = $(this).attr('data-id');
                        $('#main_container').hide('blind', function() {
                                
                                $.post('orders/create.php', {'patient': patient_id},
                                function(r) {
                                        $('#main_container').html(r);
                                        $('#main_container').show('blind');
                                });
                        });
                }
        });

        $(document).on('click', '.order_test_item', function(e) {
                $(this).hide('blind');
                $.post('tests/search.php', $(this).attr('data_id'), function(r) {
                        var test_item = $('.order_test_template').clone();
                        test_item.children('.name').text("asdf");
                        test_item.removeClass('order_test_template').addClass('order_test_added');
                        test_item.insertBefore($('#order_total_tests'));
                        test_item.show('blind');
                        getOrderPrice();
                });
        });
        $(document).on('click', '.remove_test', function(e) {
                $('.order_test_item[data-id="' + $(this).closest('.order_test_added').attr('data-id') + '"]').show('blind');
                $(this).closest('.order_test_added').remove();
                getOrderPrice();
        });
        $(document).on('change', '#order_price', function(e) {
                getOrderPrice();
        });
        
});

function getOrderPrice() {
        $('#order_total_tests').children('.total').text('0.00');
}
