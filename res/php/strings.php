<?php 
define('LABNAME_LONG', 'Bioclinic Laboratory');
define('LABNAME_SHORT', 'BL');

//Top Menu bar
define('TOPMENUBAR_HOME', 'Home');
define('TOPMENUBAR_PATIENTS', 'Patients');
define('TOPMENUBAR_ORDERS', 'Orders');
define('TOPMENUBAR_ORDERS_HISTORY', 'History');

define('TOPMENUBAR_ADMIN', 'Administration');

define('TOPMENUBAR_USERS', 'Users');
define('TOPMENUBAR_USERS_USERNAME', 'Username');
define('TOPMENUBAR_USERS_PASSWORD', 'Password');
define('TOPMENUBAR_USERS_LOGIN', 'Login');
define('TOPMENUBAR_USERS_LOGOUT', 'Logout');
define('TOPMENUBAR_USERS_LOGGING_OUT', 'Logging Out');
define('TOPMENUBAR_USERS_ACCOUNT', 'My Account');

define('PATIENT_AFILIATED', 'Afiliated');
define('PATIENT_AFILIATED_CODE', 'Afiliation Code');
define('PATIENT_FNAMES', 'First Name');
define('PATIENT_LNAMES', 'Last Name');
define('PATIENT_AGE', 'Age');
define('PATIENT_ADDRESS', 'Address');
define('PATIENT_PHONE', 'Phone Number');
define('PATIENT_SAVE_NEW', 'Save as new patient');

define('ORDER_HOSPITAL', 'Hospital');
define('ORDER_DOCTOR', 'Doctor');
define('ORDER_PRICE', 'Price');
define('ORDER_SUBMIT', 'Create Order');

define('MISC_CLOSE', 'Close');
define('MISC_SEARCH', 'Search');
define('MISC_SEARCH_NO_RESULTS', "Didn't found any register.");
?>
