<?php require_once('res/php/session.php');
        require_once('res/php/database.php');
        require_once('res/php/strings.php');?>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<title>Laboratory Control</title>

	<link href="res/css/bootstrap.min.css" rel="stylesheet">
	<link href="res/css/layout.dev.css" rel="stylesheet">
</head>
<body>
        <div id="modals_wrapper">
                <div class="modal fade" id="login_modal" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                        <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title"><?php echo TOPMENUBAR_USERS_LOGIN; ?></h4>
                                        </div>
                                        <form class="login_form" action="#">
                                                <div class="modal-body">
                                                        <div class="login_success alert alert-success" role="alert"></div>
                                                        <div class="login_warning alert alert-danger" role="alert"></div>
                                                        <div class="form-group">
                                                                <input type="text" name="username" placeholder="<?php echo TOPMENUBAR_USERS_USERNAME; ?>">
                                                                <input type="password" name="password" placeholder="<?php echo TOPMENUBAR_USERS_PASSWORD; ?>">
                                                        </div>
                                                </div>
                                                <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo MISC_CLOSE; ?></button>
                                                        <button type="submit" class="btn btn-primary login_submit"><?php echo TOPMENUBAR_USERS_LOGIN; ?></button>
                                                </div>
                                        </form>
                                </div>
                        </div>
                </div>
                <div class="modal fade" id="logout_modal" tabindex="-1" role="dialog">
                        <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                        <div class="modal-body">
                                                <h2><?php echo TOPMENUBAR_USERS_LOGGING_OUT; ?>&hellip;</p>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">
			        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			                <span class="sr-only">Toggle navigation</span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><?php echo LABNAME_SHORT; ?></a>
			</div>

			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			        <ul class="nav navbar-nav">
        			        <li><a href="#"><?php echo TOPMENUBAR_HOME; ?><span class="sr-only">(current)</span></a></li>
                			<li class="search_patient_navbar"><a href="#"><?php echo TOPMENUBAR_PATIENTS; ?></a></li>
			                <li class="dropdown">
                        			<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo TOPMENUBAR_ORDERS; ?> <span class="caret"></span></a>
        		                        <ul class="dropdown-menu">
        		                                <li><a href="#"><?php echo TOPMENUBAR_ORDERS_HISTORY; ?></a></li>
                                			<li role="separator" class="divider"></li>
                                			<li><a href="#">Separated link</a></li>
                                			<li role="separator" class="divider"></li>
			                        </ul>
			                </li>
			        </ul>
			        <form class="navbar-form navbar-left">
			                <div class="form-group">
			                        <input type="text" class="form-control" placeholder="Search">
			                </div>
	                		<button type="submit" class="btn btn-default">Submit</button>
               			</form>
		        	<ul class="nav navbar-nav navbar-right">
		        	        <?php
		        	                if(!isset($_SESSION['user']['username'])) {
		        	        ?>
		        	        <li class="login_trigger" data-toggle="modal" data-target="#login_modal"><a href="#login"><?php echo TOPMENUBAR_USERS_LOGIN; ?></a></li>
		        	        <?php } else { ?>
			                <li class="dropdown">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo isset($_SESSION['username']) ? $_SESSION['username'] : TOPMENUBAR_ADMIN; ?> <span class="caret"></span></a>
			                        <ul class="dropdown-menu">
			                                <li><a href="#"><?php echo TOPMENUBAR_USERS; ?></a></li>
			                                <li><a href="#"><?php echo TOPMENUBAR_USERS_ACCOUNT; ?></a></li>
			                                <li role="separator" class="divider"></li>
			                                <li class="logout_trigger"><a href="#"><?php echo TOPMENUBAR_USERS_LOGOUT; ?></a></li>
			                        </ul>
		                	</li>
		                	<?php } ?>
		        	</ul>
			</div>
		</div>
	</nav>
	
        <div id="main_container" class="container">
                
        </div>
	<script src="res/js/jquery.min.js"></script>
	<script src="res/js/tether.min.js"></script>
	<script src="res/js/bootstrap.min.js"></script>
	<script src="res/js/global.dev.js"></script>
</body>
</html>
