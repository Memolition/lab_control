<?php require_once('../res/php/session.php');
        require_once('../res/php/database.php');
        require_once('../res/php/strings.php');

if(isset($_POST['username']) && !empty($_POST['username'])) {
        if($login_statement = $MySQLi->prepare("SELECT username, password, fname, lname FROM users WHERE username = ?")) {
                $login_statement->bind_param("s", $_POST['username']);
                $login_statement->execute();
                $login_statement->bind_result($username, $password, $fname, $lname);
                if($login_statement->fetch()) {
                        if(isset($_POST['password']) && !empty($_POST['password'])) {
                                if(password_verify($_POST['password'], $password)) {
                                        $_SESSION['user']['username'] = $username;
                                        $_SESSION['user']['fname'] = $fname;
                                        $_SESSION['user']['lname'] = $lname;
                                        echo "0Successfully logged in as " . $fname . " " . $lname . ".";
                                } else {
                                        echo "1Wrong password submitted, check your information and try again.";
                                }
                        }
                } else {
                        echo "1No user registered with that username";
                }
        } else {
                echo "1An error ocurred, please review and try submitting your information again./r/nIf the error persists, please contact the administrator.";
        }
}

?>
