<?php require_once('../res/php/session.php');
        require_once('../res/php/database.php');
        require_once('../res/php/strings.php');

$error = false;
$username = $fname = $lname = $password = "";

if(isset($_POST['username']) && !empty($_POST['username'])) {
        $username = $_POST['username'];
} else {
        $error = true;
}

if(isset($_POST['password']) && !empty($_POST['password'])) {
        if(!$password = password_hash($_POST['password'], PASSWORD_BCRYPT)) {
                echo "An error ocurred, please review and try submitting your information again.\r\nIf the error persists, please contact the administrator.";
                $error = true;
        }
} else {
        $error = true;
        echo "An error ocurred, please review and try submitting your information again.\r\nIf the error persists, please contact the administrator.";
}

if(isset($_POST['fname']) && !empty($_POST['fname'])) {
        $fname = $_POST['fname'];
} else {
        $error = true;
        echo "An error ocurred, please review and try submitting your information again.\r\nIf the error persists, please contact the administrator.";
}

if(isset($_POST['lname']) && !empty($_POST['lname'])) {
        $lname = $_POST['lname'];
} else {
        $error = true;
        echo "An error ocurred, please review and try submitting your information again.\r\nIf the error persists, please contact the administrator.";
}

if(!$error) {
        if($insert_user_statement = $MySQLi->prepare('INSERT INTO users (username, fname, lname, password) VALUES (?, ?, ?, ?)')) {
                $insert_user_statement->bind_param("ssss", $username, $fname, $lname, $password);
                $insert_user_statement->execute();
                $insert_user_statement->close();
                echo "Information processed successfully";
        } else {
                echo "An error ocurred, please review and try submitting your information again.\r\nIf the error persists, please contact the administrator.";
        }
} else {
        echo 'An error ocurred, please make sure you fill all of the required fields and try again.';
}
?>
