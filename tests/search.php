<?php require_once('../res/php/session.php');
        require_once('../res/php/database.php');
        require_once('../res/php/strings.php');

if(isset($_POST['test']) && !empty($_POST['test'])) {
        $test_id = $_POST['test'];
        if($search_statement = $MySQLi->prepare("SELECT name, prices FROM tests WHERE id = ?")) {
                $search_statement->bind_param('i', $test_id);
                $search_statement->execute();
                $search_statement->bind_result($name, $prices);
                $search_statement->fetch();
                echo '{"name":"' . $name . '", "prices":"' . $prices . '"}';
                $search_statement->close();
        }

}
?>
